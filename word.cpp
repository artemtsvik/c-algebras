#include<string>
#include<vector>
#include<iostream>

#include "word.hpp"



Word::Word(double q) 
{
	this->q = q;
}

Word::Word(unsigned long* indexes, bool* is_ivolutioned, unsigned long length, double q)
{
	this->q = q;
	symbol s;
	for (unsigned long i = 0; i != length; ++i)
	{
		s.index = indexes[i];
		s.is_inv = is_ivolutioned[i];
		this->symbols.push_back(s);
	}
}

void mull_words(Word* result, Word& a, Word& b)
{
	std::vector<symbol>::iterator a_r = prev(a.symbols.end()), b_l = b.symbols.begin();
	while (a_r->is_inv && !(b_l->is_inv) && a_r->index == b_l->index)
	{
		--a_r;
		++b_l;
	}

	result->symbols.insert(result->symbols.end(), a.symbols.begin(), ++a_r);
	result->symbols.insert(result->symbols.end(), b_l, b.symbols.end());
}

void canocic_form(Word* result, Word& a)
{
	long long i = 1, j = a.symbols.size();
	std::vector<symbol>::iterator a_end = a.symbols.end();
	for (std::vector<symbol>::iterator s_to_right = a.symbols.begin(), s_to_left = a_end;
		s_to_right != a_end; ++s_to_right, ++i)
	{
		--s_to_left;

 		if (s_to_right->is_inv)
		{
			result->symbols.push_back(*s_to_right);
			result->coefficient *= pow(result->q,j-i);
			--j;
		}

		if (!(s_to_left->is_inv))
		{
			result->symbols.insert(result->symbols.begin(), *s_to_left);
		}
	}
}

std::string Word::get_str()
{
	std::string res = "";
	if (this->coefficient != 1)
	{
		res += std::to_string(this->coefficient);
	}
	std::vector<symbol>::iterator word_end = this->symbols.end();
	for (std::vector<symbol>::iterator s = this->symbols.begin(); s != word_end; ++s)
	{
		res += "S_{" + std::to_string((*s).index) + "}";
		if ((*s).is_inv)
		{
			res += "^*";
		}
	}
	return res;
}
