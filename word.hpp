#include<string>
#include<vector>

#ifndef WORD_HPP
#define WORD_HPP


typedef struct
{
	long long index;
	bool is_inv = false;
} symbol;

class Word
{
public:
	double q;
	double coefficient = 1;
	std::vector<symbol> symbols;

	Word(double q);

	Word(unsigned long* indexes, bool* is_ivolutioned, unsigned long length, double q);

	std::string get_str();
};

void mull_words(Word* result, Word& a, Word& b);

void canocic_form(Word* result, Word& a);

#endif