#include<iostream>
#include "word.hpp"


int main()
{
	std::cout.precision(16);
	std::cout << "Test 1" << std::endl;
	unsigned long inds_a1[] = { 1,2,3,4 },
		inds_b1[] = { 4,3,4,4 };
	bool invs_a1[] = { false, true, true, true },
		invs_b1[] = { false, false, false, true };



	Word a1 = Word(inds_a1, invs_a1, 4, 0.5), b1 = Word(inds_b1, invs_b1, 4, 0.5), c1 = Word(0.5);
	mull_words(&c1, a1, b1);

	std::cout << "a1 = " << a1.get_str() << std::endl << "b1 = " << b1.get_str() << std::endl
		<< "a1*b1 = " << c1.get_str() << std::endl << std::endl;


	std::cout << "Test 2" << std::endl;
	unsigned long inds_a2[] = { 1,2,3,4 },
		inds_b2[] = { 4,3,4,4 };
	bool invs_a2[] = { false, true, false, true },
		invs_b2[] = { false, false, false, true };



	Word a2 = Word(inds_a2, invs_a2, 4, 0.5), b2 = Word(inds_b2, invs_b2, 4, 0.5), c2 = Word(0.5);
	mull_words(&c2, a2, b2);

	std::cout << "a2 = " << a2.get_str() << std::endl << "b2 = " << b2.get_str() << std::endl
		<< "a2*b2 = " << c2.get_str() << std::endl << std::endl;

	std::cout << "Test 3" << std::endl;
	unsigned long inds_a3[] = { 1,2,3,4 },
		inds_b3[] = { 4,3,4,4 };
	bool invs_a3[] = { false, true, false, false },
		invs_b3[] = { true, false, false, true };



	Word a3 = Word(inds_a3, invs_a3, 4, 0.5), b3 = Word(inds_b3, invs_b3, 4, 0.5), c3 = Word(0.5);
	mull_words(&c3, a3, b3);

	std::cout << "a3 = " << a3.get_str() << std::endl << "b3 = " << b3.get_str() << std::endl
		<< "a3*b3 = " << c3.get_str() << std::endl << std::endl;


	std::cout << "Test 4" << std::endl;
	unsigned long inds_a4[] = { 1,2,3,4,5,6,7 };
	bool invs_a4[] = { false, true, true, false, true, false, true };



	Word a4 = Word(inds_a4, invs_a4, 7, 0.5), c4 = Word(0.5);
	canocic_form(&c4, a4);

	std::cout << "a4 = " << a4.get_str() << std::endl
		<< "Canonic form: " << c4.get_str() << std::endl << std::endl;


	return 0;
}